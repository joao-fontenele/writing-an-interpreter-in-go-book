# Get the currently used golang install path (in GOPATH/bin, unless GOBIN is set)
ifeq (,$(shell go env GOBIN))
GOBIN=${PWD}/bin
else
GOBIN=$(shell go env GOBIN)
endif

# Setting SHELL to bash allows bash commands to be executed by recipes.
# Options are set to exit when a recipe line exits non-zero or a piped command fails.
SHELL = /usr/bin/env bash -o pipefail
.SHELLFLAGS = -ec

all: run-docker

# Location to install dependencies to
LOCALBIN ?= $(shell pwd)/bin
$(LOCALBIN):
	mkdir -p $(LOCALBIN)

.PHONY: install-tools
install-tools: $(LOCALBIN)
	cd tools ; PATH=${PATH}:${GOBIN} GOBIN=${GOBIN} ./install.sh

.PHONY: dlv
dlv: gomodtidy
	CGO_ENABLED=0 go build -gcflags="all=-N -l" -o ./bin/app main.go
	@echo connect to the debugger from the IDE
	$(LOCALBIN)/dlv --listen=:2345 --headless=true --api-version=2 --accept-multiclient exec ./bin/app -- server

.PHONY: run
run: gomodtidy
	$(LOCALBIN)/reflex -d none -s -r '\.go$$' go run main.go server

.PHONY: build
build:
	builder=$(shell docker buildx create --use)
	docker buildx build --push --platform linux/arm64/v8,linux/amd64 --tag joao-fontenele/demo-app:$(shell git rev-parse HEAD) .
	docker buildx rm $(builder)

.PHONY: fmt
fmt:
	@if goimports -l -w . | grep . ; then \
		echo "goimports found files that need to be formatted"; \
		exit 1; \
	fi

.PHONY: lint
lint:
	$(LOCALBIN)/golangci-lint run ./...
	go vet ./...

.PHONY: test
test:
	go test ./...

.PHONY: gomodtidy
gomodtidy:
	go mod tidy

.PHONY: govulncheck
govulncheck:
	$(LOCALBIN)/govulncheck ./...

.PHONY: ci
ci: install-tools fmt lint test

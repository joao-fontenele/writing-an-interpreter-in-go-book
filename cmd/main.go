package main

import (
	"fmt"
	"os"

	"gitlab.com/joao-fontenele/writing-an-interpreter-in-go-book/repl"
)

func main() {
	fmt.Printf("Feel free to type in commands\n")
	repl.Start(os.Stdin, os.Stdout)
}

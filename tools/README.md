# Tools Versioning

Use this module to control versions of project binaries.

## Add a new tool

```bash
# ./add <tool without version>
# this will install the latest while versioning in go.mod, e.g:
./add github.com/spf13/cobra-cli
```

## Install tools

```bash
./install.sh
```

#!/bin/bash

set -euxo pipefail

if [[ -z "${GOBIN+x}" ]]; then
  echo "please set GOBIN first"
  exit 1
fi


# leave out versions to use the ones specified in go.mod

# add new tools below
go install github.com/go-delve/delve/cmd/dlv
go install github.com/cespare/reflex
go install golang.org/x/vuln/cmd/govulncheck
go install golang.org/x/tools/cmd/goimports


# official docs recommended installing like this instead of using traditional go install command
GOLANGCI_LINT_VERSION=v1.57.2
if which golangci-lint >/dev/null; then
  echo "golangci-lint is already installed"
else
  curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b "${GOBIN}" ${GOLANGCI_LINT_VERSION}
fi

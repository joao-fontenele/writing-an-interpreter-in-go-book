//go:build tools
// +build tools

package tools

// see:
// - https://github.com/go-modules-by-example/index/blob/master/010_tools/README.md
// - https://github.com/MarioCarrion/todo-api-microservice-example/blob/e44dc3d4016b80b8102eb1214fea452aebdd2667/internal/tools/tools.go

import ( // add new tools below
	_ "github.com/cespare/reflex"
	_ "github.com/go-delve/delve/cmd/dlv"
	_ "golang.org/x/tools/cmd/goimports"
	_ "golang.org/x/vuln/cmd/govulncheck"
)
